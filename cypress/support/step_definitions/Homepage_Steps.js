import { Given, When } from "@badeball/cypress-cucumber-preprocessor";

const url = "http://www.webdriveruniversity.com/";

Given(`I navigate to the webd homepage`, () => {
    cy.clearLocalStorage()
    cy.visit(url)
})

When(`I click on the contact us button`, () => {
    cy.get("#contact-us").invoke("removeAttr", "target").click();
})
